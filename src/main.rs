//#![deny(warnings)]
mod mediacrawler;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::path::PathBuf;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use hyper::client::HttpConnector;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Client, Method, Request, Response, Server, StatusCode};
use structopt::StructOpt;
use mediacrawler::{Directory, Image};
type GenericError = Box<dyn std::error::Error + Send + Sync>;
type Result<T> = std::result::Result<T, GenericError>;
#[derive(StructOpt, Clone)]
struct Args {
    #[structopt(help = "path to a picture directory")]
    images_path: PathBuf,
}

static INTERNAL_SERVER_ERROR: &[u8] = b"Internal Server Error";
static NOTFOUND: &[u8] = b"Not Found";
static TD_ITEM: &str = r#"<tr><td><a href="list/{{PATH}}">{{PATH}}</a></td></tr>"#;
//static TABLE_START: &str = r#"<table>"#;
//static TABLE_END: &str = "</table>";

async fn image_page(aid: &str, iid: &str) -> Result<Response<Body>> {
    if let Ok(mut file) = File::open("templates/image_page.html").await {
        let mut buf = Vec::new();
        if let Ok(_) = file.read_to_end(&mut buf).await {
            let buf = String::from_utf8_lossy(&buf).replace("{{ALBUM_ID}}", aid).replace("{{IMAGE_ID}}", iid);
            return Ok(Response::new(buf.into()));
        }
        return Ok(Response::builder()
           .status(StatusCode::INTERNAL_SERVER_ERROR)
           .body(INTERNAL_SERVER_ERROR.into())
           .unwrap());
    }

    Ok(Response::builder()
       .status(StatusCode::NOT_FOUND)
       .body(NOTFOUND.into())
       .unwrap())
}

async fn list_root(crawl: Arc<Mutex<Directory>>) -> Result<Response<Body>> {
    let res = crawl.lock().unwrap().directories("").unwrap_or(String::new());
    Ok(Response::builder().status(StatusCode::OK).body(res.into()).unwrap())
}

async fn send_image(filename: PathBuf) -> Result<Response<Body>> {
    // Serve a file by asynchronously reading it entirely into memory.
    // Uses tokio_fs to open file asynchronously, then tokio::io::AsyncReadExt
    // to read into memory asynchronously.

    if let Ok(mut file) = File::open(filename).await {
        let mut buf = Vec::new();
        if let Ok(_) = file.read_to_end(&mut buf).await {
            return Ok(Response::new(buf.into()));
        }
        return Ok(Response::builder()
           .status(StatusCode::INTERNAL_SERVER_ERROR)
           .body(INTERNAL_SERVER_ERROR.into())
           .unwrap());
    }

    Ok(Response::builder()
       .status(StatusCode::NOT_FOUND)
       .body(NOTFOUND.into())
       .unwrap())
}

async fn send_file(filename: &str) -> Result<Response<Body>> {
    // Serve a file by asynchronously reading it entirely into memory.
    // Uses tokio_fs to open file asynchronously, then tokio::io::AsyncReadExt
    // to read into memory asynchronously.

    if let Ok(mut file) = File::open(filename).await {
        let mut buf = Vec::new();
        if let Ok(_) = file.read_to_end(&mut buf).await {
            return Ok(Response::new(buf.into()));
        }
        return Ok(Response::builder()
           .status(StatusCode::INTERNAL_SERVER_ERROR)
           .body(INTERNAL_SERVER_ERROR.into())
           .unwrap());
    }

    Ok(Response::builder()
       .status(StatusCode::NOT_FOUND)
       .body(NOTFOUND.into())
       .unwrap())
}

async fn request(req: Request<Body>, crawl: Arc<Mutex<Directory>>) -> Result<Response<Body>> {
    let uri = &req.uri().path();
    if uri.starts_with("/albums/") == true {
        let mut res = String::new();
        let s: Vec<&str> = uri.split("/").collect();
        println!("{:?}", s);
        for id in &s[2..] {
            println!("{}", id);
            res = crawl.lock().unwrap().directories(id).unwrap_or(res); 
            break; 
        }
        return Ok(Response::builder().status(StatusCode::OK).body(res.into()).unwrap());
    }
    if uri.starts_with("/album/") == true {
        let mut res = String::new();
        let s: Vec<&str> = uri.split("/").collect();
        for id in &s[2..] {
            println!("{}", id);
            res = crawl.lock().unwrap().images(id).unwrap_or(res); 
            break; 
        }
        return Ok(Response::builder().status(StatusCode::OK).body(res.into()).unwrap());
    }
    if uri.starts_with("/image/") == true {
        let s: Vec<&str> = uri.split("/").collect();
        if s.len() > 2 {
            let aid = &s[2];
            let mut iid = "";
            if s.len() > 3 {
                iid = &s[3];
            }
            return image_page(aid, iid).await;
        }
    }

    if uri.starts_with("/download/") == true {
        let mut file: Option<PathBuf> = None;
        let s: Vec<&str> = uri.split("/").collect();
        println!("{:?}", s);
        for id in &s[2..] {
            println!("{}", id);
            file = crawl.lock().unwrap().image_path(id); 
            break; 
        }
        if let Some(file) = file {
            return send_image(file).await;
        }
    }

    // Return 404 not found response.
    Ok(Response::builder()
       .status(StatusCode::NOT_FOUND)
       .body(NOTFOUND.into())
       .unwrap())

}

async fn responses(
    req: Request<Body>,
    _client: Client<HttpConnector>,
    crawl: Arc<Mutex<Directory>>
) -> Result<Response<Body>> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/albums") => list_root(crawl).await,
        (&Method::GET, "/stylesheet.css") => send_file("templates/stylesheet.css").await,
        (&Method::GET, _) => {
            request(req, crawl).await
        }
        _ => {
            Ok(Response::builder()
               .status(StatusCode::NOT_FOUND)
               .body(NOTFOUND.into())
               .unwrap())
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::from_args();
    let client = Client::new();
    let mut media = Directory::new(args.images_path.clone());
    media.crawl().expect("Failed to crawl image directory");
    println!("{}", serde_json::to_string(&media).unwrap_or(format!("serialize failed")));
    let media = Arc::new(Mutex::new(media));

    let addr = "127.0.0.1:4444".parse().unwrap();
    let media2 = Arc::clone(&media);
    let new_service = make_service_fn(move |_| {
        // Move a clone of `client` into the `service_fn`.
        let client = client.clone();
        let media = media2.clone();
        async {
            Ok::<_, GenericError>(service_fn(move |req| {
                // Clone again to ensure that client outlives this closure.
                responses(req, client.to_owned(), media.to_owned())
            }))
        }
    });
    let server = Server::bind(&addr).serve(new_service);
    println!("Listening on http://{}", addr);

    server.await?;
    Ok(())
}

