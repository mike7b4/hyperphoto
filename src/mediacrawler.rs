#[macro_use]
use serde::{Serialize};
use std::fmt;
use std::fs;
use std::path::PathBuf;
use std::collections::HashMap;
use sha2::{Sha256, Digest};

#[derive(Debug)]
pub enum Error {
    IO(std::io::Error),
}

#[derive(Serialize, Debug, Clone)]
pub struct Image {
    id: String,
    file_name: String,
}

impl Image {
    fn new(path: PathBuf) -> Self {
        let file_name = String::from(path.file_name().unwrap().to_str().unwrap_or(""));
        let mut sha = Sha256::default();
        sha.input(&path.to_str().unwrap().as_bytes());
        let mut f = Self {
            id: String::with_capacity(64),
            file_name
        };

        for byte in sha.result() {
            f.id += &format!("{:02x}", byte);
        }

        f
    }
}

impl fmt::Display for Image {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut s = format!("id: {}", self.id);
        s += &format!("file: {}\n", self.file_name);
        write!(f, "{}", s)
    }
}

struct Images {
    
}

#[derive(Serialize, Debug)]
pub struct Directory {
    id: String,
    name: String,
    #[serde(skip)]
    directory: PathBuf,
    // files in current directory
    //#[serde(skip)]
    images: Vec<Image>,
    directories: HashMap<String, Directory>,
//    images_shas: HashMap<String, &'a Image>
}

impl Directory {
    pub fn new(path: PathBuf) -> Self {
        let mut sha = Sha256::default();
        sha.input(&path.to_str().unwrap().as_bytes());
        let mut d = Self {
            id: String::with_capacity(64),
            name: String::from(path.file_name().unwrap().to_str().unwrap_or("?")),
            directory: path,
            images: Vec::new(),
            directories: HashMap::new(),
        };

        for byte in sha.result() {
            d.id += &format!("{:02x}", byte);
        }

        d
    }

    pub fn crawl(&mut self) -> Result<(), Error> {
        for entry in fs::read_dir(&self.directory).map_err(|e| Error::IO(e))? {
            let dir = entry.map_err(|e| Error::IO(e))?;
            if dir.metadata().map_err(|e| Error::IO(e))?.is_dir() {
                let mut dir = Directory::new(dir.path());
                match dir.crawl() {
                    Ok(()) => {
                        self.directories.insert(dir.id.clone(), dir);
                    }
                    Err(e) => {
                        eprintln!("{:?}", e);
                    }
                };
            } else {
                match dir.path().extension() {
                    Some(ext) => {
                        match &ext.to_str().unwrap_or("") {
                            &"jpg" | &"JPG" | &"jpeg" | &"JPEG" => {
                                let image = Image::new(dir.path());
                                self.images.push(image);
                            }
                            _ => {}
                        }
                    }
                    None => {}
                }
            }
        }

        self.images.sort_by(|a, b| a.file_name.cmp(&b.file_name));
        Ok(())
    }

    pub fn directories(&self, id: &str) -> Option<String> {
        let mut dir = self;
        if id.len() > 0 {
            dir = self.directories.get(id)?;
        }

        if let Ok(s) = serde_json::to_string(dir) {
            return Some(s);
        }
        None
    }

    pub fn images(&self, id: &str) -> Option<String> {
        let mut dir = self;
        if id.len() > 0 {
            dir = self.directories.get(id)?;
        }

        if let Ok(s) = serde_json::to_string(&dir.images) {
            return Some(s);
        }
        None
    }

    // Lookup file
    pub fn image_path(&self, id: &str) -> Option<PathBuf> {
        for image in &self.images {
            if image.id == id {
                let mut path = self.directory.clone();
                path.push(image.file_name.clone());
                return Some(path);
            }
        }

        for (_, dir) in &self.directories {
            if let Some(path) = dir.image_path(&id) {
			println!("{:?}", path);
                return Some(path);
            }
        }

        None
    }
}

