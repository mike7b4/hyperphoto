# Hyperphoto

Just a simple POC of an "local" imageviewer and show it in your web browser.
I hope to expand this to be my own tiny "nextcloud solution".

# Dependies

 - hyper with async support.
 - structopt
 - Rust >= v1.40

# How to run it

```cargo run -- <PATH_TO_YOUR_LOCAL_PHOTOS>```

Then point your browser to localhost:4444/image/

# Todo

 - [ ] Create image thumbnails

 - [ ] bind to an network interface instead of localhost.

